# Multiple GIT accounts setup

This short tutorial shows how to set up multiple different accounts of many git repository sites using SSH, all this tutorial and its commands were aplied in Ubuntu.

### Generate SSH keys
Generate SSH keys for each one of the sites you wanna connect:

1. Open your terminal and type the following command to generate a RSA key:
    ```sh
    ssh-keygen -o -t rsa -b 4096 -C "email@example.com"
    ```  
    When prompted for the path of the key, put a specific name to identify the key, for example `id_rsa_gitlab`.
    
    ```
    Generating public/private rsa key pair.
    Enter file in which to save the key (/home/username/.ssh/id_rsa): /home/username/.ssh/id_rsa_gitlab
    ```
    After that when asked for password set-up one if you want or press enter to leave it blank.
    
2. In order to use the generated keys, you’ll need to save them to a configuration file. For OpenSSH clients this is configured in the `~/.ssh/config` file. In this file you can set up configurations for multiple hosts, like GitLab, GitHub, Bitbucket, etc.

    If there is not any `config` file create it executing `touch config` and edit it (via nano or any text editor) following the example below.
    ```
    # GitLab.com
    Host gitlab.com
      Preferredauthentications publickey
      IdentityFile ~/.ssh/id_rsa_gitlab
    
    # Github.com
    Host github.com
      Preferredauthentications publickey
      IdentityFile ~/.ssh/id_rsa_github
    ```
3. Add the SSH keys in the on gitlab sites : https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html

5. To test the connection (in this case to gitlab) use the following command:
    ```
    ssh -T git@gitlab.com
    ```
    If all things are OK the server returns the following message:
    > Welcome to GitLab, @username! message.
    
    If there is a problem authenticating try to run the last command with the verbose argument to check out the details of the process: 
    
    ```
    ssh -vT git@gitlab.com
    ```
    
    Also try to start the **ssh-agent** using the following command
    ```
    eval $(ssh-agent -s)
    ssh-add ~/.ssh/other_id_rsa
    ```
    
